package com.example.demo.models;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Blogs {

    @Id
    private String name;

    private String link;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

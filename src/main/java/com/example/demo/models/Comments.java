package com.example.demo.models;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Comments {

    @Id
    @GeneratedValue(strategy =  GenerationType.AUTO)
    private Integer nr;

    private Integer approved;

    private String page_commented;

    private  String comment;

    private  String id;

    private  String email;

    private String name;

    private LocalDate data;

    public Integer getNr() {
        return nr;
    }

    public void setNr(Integer nr) {
        this.nr = nr;
    }

    public Integer getApproved() {
        return approved;
    }

    public void setApproved(Integer approved) {
        this.approved = approved;
    }

    public String getPage_commented() {
        return page_commented;
    }

    public void setPage_commented(String page_commented) {
        this.page_commented = page_commented;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getList() {
        return "In <a th:href=@{/getPage(requestPage=${"+ this.page_commented +"})}>+ "+this.page_commented+"</a>"+this.comment.substring(10)+" by "+this.name;
    }
}

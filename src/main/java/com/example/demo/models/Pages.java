package com.example.demo.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.Comparator;

@Entity
public class Pages implements Comparable<Pages> {

    @Id
    private String title;

    private String photo;

    @NotBlank(message = "label should not be blank")
    private String label;

    public Integer nr_art;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Pages() {
    }

    public Pages(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getNr_art() {
        return nr_art;
    }

    public void setNr_art(Integer nr_art) {
        this.nr_art = nr_art;
    }


    @Override
    public int compareTo(Pages o) {
        return this.nr_art - o.nr_art;
    }
}


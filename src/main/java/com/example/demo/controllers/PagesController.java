package com.example.demo.controllers;

import com.example.demo.models.*;
import com.example.demo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.example.demo.controllers.PagesController.FlorinGiurca.MEDIA_RESS;
import static com.example.demo.controllers.PagesController.FlorinGiurca.PAGES;

@Controller
public class PagesController {

    @Autowired
    private PagesRepository pagesRepository;

    @Autowired
    private BooksRepository booksRepository;

    @Autowired
    private QuotesRepository quotesRepository;

    @Autowired
    private CommentsRepository commentsRepository;

    @Autowired
    private BlogsRepository blogsRepository;

    @Autowired
    private PasswordRepository passwordRepository;

    @GetMapping
    public String dataBinding(Model model) {

        // aici se intra din pagina de index
        // se va afisa ultima pagina adaugata in baza de date

        List<Pages> pages = pagesRepository.findAll();
        if (!pages.isEmpty()) {
            Collections.sort(pages);
            Integer nrPages = pages.size();
            String lastPage = pages.get(nrPages - 1).getTitle();
            String lastImage = pages.get(nrPages - 1).getPhoto();
            prepareModelAttributes(model, pages, lastPage, lastImage);
        }

        return "index";
    }

    @GetMapping("/getPage")
    public String getPage(Model model, String requestPage, String requestImage) {

        // aici se intra cand este apelata o pagina din lista din bara laterala
        // se va afisa pagina apelata

        List<Pages> pages = pagesRepository.findAll();
        List<Books> books = booksRepository.findAll();
        List<Quotes> quotes = quotesRepository.findAll();
        List<Comments> comments = commentsRepository.findAll();
        List<Blogs> blogs = blogsRepository.findAll();
        String lastPage = requestPage;
        prepareModelAttributes(model, pages, lastPage, requestImage);
        return "index";
    }

    private void prepareModelAttributes(Model model, List<Pages> pages, String lastPage, String lastImage) {
        List<Books> books = booksRepository.findAll();
        List<Quotes> quotes = quotesRepository.findAll();
        List<Comments> comments = commentsRepository.findAll();
        List<Blogs> blogs = blogsRepository.findAll();


        Integer nrQuotes = quotes.size();
        if (nrQuotes > 0) {
            Random random = new Random();
            Integer randomInt = random.nextInt(nrQuotes);
            String qLink = "<div ><img src=" + FlorinGiurca.MEDIA_RESS + quotes.get(randomInt).getPhoto() + "></div>";
            String qText = quotes.get(randomInt).getQuote();

            model.addAttribute("qLink", qLink);
            model.addAttribute("qText", qText);
        }


        for (Books books1 : books) {
            String temp = "<div class=\"zoom\"><img src=" + FlorinGiurca.MEDIA_RESS + books1.getCover() + "></div>";
            books1.setCover("<div><a href=" + books1.getEditura() + "target = a>" + temp + "</div></a><br />");
        }

        Integer nrComments = comments.size();
        Integer lastTenComments = nrComments <= 10 ? 0 : nrComments - 10;

        List<Comments> listOfComments = new ArrayList<>();
        for (Integer i = lastTenComments; i < nrComments; i++) {
            if (comments.get(i).getApproved() == 1) {
                listOfComments.add(comments.get(i));
            }
        }

        int nrComPage = 0;
        List<Comments> thisPageComments = new ArrayList<>();
        for (Integer i = 0; i < nrComments; i++) {
            if (comments.get(i).getPage_commented().equals(lastPage) && comments.get(i).getApproved() == 1) {
                thisPageComments.add(comments.get(i));
                nrComPage++;
            }
        }

        String lastPageLink = "<a href=" + PAGES + lastPage + ".htm target=a>" + lastPage + "</a>";
        String lastPageContent = "<iframe src=\"" + lastPage + ".htm\" title=" + lastPage + " width=100% height=5000 border = none onload=\"iFrameLoaded()\" id=\"main-frame\"></iframe>";
        String lastPagePhoto = "<img src=\"" + lastImage + ".jpg\" width=100% />";
        String aboutLink = "<a href=" + PAGES + "Despre.htm target=a><i>DESPRE</i></a>";
        String newrule11 = "<img src = " + MEDIA_RESS + "newrule11.gif  border=0 />";
        String faceBookLogo = " <a href=\"https://www.facebook.com/florin.giurca.7\" target=1><img alt=\"vezi-ma pe facebook\" src = " + MEDIA_RESS + "facebook_logo.jpg  border=0 ></a>";
        String topButton = "<a href=\"#start\"><img src = " + MEDIA_RESS + "buton_blog.gif  border=0 /></a>";

        model.addAttribute("lastPageLink", lastPageLink);
        model.addAttribute("lastPage", lastPage);
        model.addAttribute("lastPageContent", lastPageContent);
        model.addAttribute("lastPagePhoto", lastPagePhoto);
        model.addAttribute("aboutLink", aboutLink);
        model.addAttribute("newRule", newrule11);
        model.addAttribute("faceBookLogo", faceBookLogo);
        model.addAttribute("topButton", topButton);
        model.addAttribute("books", books);
        model.addAttribute("pages", pages);
        model.addAttribute("listOfComments", listOfComments);
        model.addAttribute("thisPageComments", thisPageComments);
        model.addAttribute("nrComPage", nrComPage);
        model.addAttribute("blogs", blogs);
    }

    @GetMapping("/admin")
    public String getAdminPage() {
        return "admin";
    }

    @GetMapping("/add-quotes")
    public String addNewQuote(Quotes quotes) {
        return "add-quotes";
    }

    @GetMapping("/add-books")
    public String addNewBook(Books books) {
        return "add-books";
    }

    @GetMapping("/add-page")
    public String addNewPage(Pages pages) {
        return "add-pages";
    }

    @GetMapping("/add-blogs")
    public String addNewBlog(Blogs blogs) {
        return "add-blogs";
    }

    @GetMapping("/add-comment")
    public String addNewComment(Model model, Comments comments, String lastPage) {
        model.addAttribute("lastPage", lastPage);
        return "add-comments";
    }

    @GetMapping("/getPassword")
    public String getPassword(Password password) {

        return "get-password";
    }

    @GetMapping("/changePassword")
    public String changePassword(Password password) {

        return "change-password";
    }

    @PostMapping("/verifyPassword")
    public String verifyPassword(Password password) {
        if (passwordRepository.existsById(password.getPassw())) {
            return "admin";
        } else {
            return "index";
        }
    }


    @GetMapping("/delete-page")
    public String deletePage(Model model) {
        List<Pages> pages = pagesRepository.findAll();
        model.addAttribute("pages", pages);
        return "delete-a-page";
    }

    @GetMapping("/moderate-comments")
    public String moderateComments(Model model) {
        List<Comments> comments = commentsRepository.findAll();
        Integer nrComments = comments.size();
        List<Comments> unModeratedComments = new ArrayList<>();
        for (Integer i = 0; i < nrComments; i++) {
            if (comments.get(i).getApproved() != 1) {
                unModeratedComments.add(comments.get(i));
            }
        }
        model.addAttribute("unModeratedComments", unModeratedComments);
        return "moderate";
    }

    @PostMapping("/saveQuote")
    public String saveQuote(Quotes quotes) {
        quotesRepository.save(quotes);
        return "redirect:/";
    }

    @PostMapping("/saveBook")
    public String saveBook(Books books) {
        booksRepository.save(books);
        return "redirect:/";
    }

    @PostMapping("/saveBlog")
    public String saveBlog(Blogs blogs) {
        blogsRepository.save(blogs);
        return "redirect:/";
    }

    @PostMapping("/savePages")
    public String savePages(Pages pages) {
//        pages.setPhoto(pages.getTitle());
        pagesRepository.save(pages);
        return "redirect:/";
    }

    @PostMapping("/saveComment")
    public String saveComment(Comments comments, String lastPage) {
        comments.setPage_commented(lastPage);
        comments.setApproved(0);
        commentsRepository.save(comments);
        return "redirect:/";
    }

    @PostMapping("/saveNewPassword")
    public String saveNewPassword(Password password1) {
        passwordRepository.deleteAll();
        passwordRepository.save(password1);
        return "redirect:/";
    }

    @GetMapping("/approve")
    public String approve(Integer nr) {
        Optional comments1 = commentsRepository.findById(nr);
        if (comments1.isPresent()) {
            Comments commExist = (Comments) comments1.get();
            commExist.setApproved(1);
            commentsRepository.save(commExist);
        }
        return "redirect:/";
    }

    @GetMapping("/removePage")
    public String removePage(String titlu) {
        Optional pageForDelete = pagesRepository.findById(titlu);
        if (pageForDelete.isPresent()) {
            pagesRepository.deleteById(titlu);
        }
        return "redirect:/";
    }


    @GetMapping("/reject")
    public String reject(Integer nr) {
        Optional comments1 = commentsRepository.findById(nr);
        if (comments1.isPresent()) {
            commentsRepository.deleteById(nr);
        }
        return "redirect:/";
    }

    static class FlorinGiurca {
        static final String PAGES = "";
        static final String MEDIA_RESS = "";
    }
}


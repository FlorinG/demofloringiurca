'use strict';

var singleUploadForm = document.querySelector('#singleUploadForm');
var htmFileUploadInput = document.querySelector('#htmFileUploadInput');
var jpgFileUploadInput = document.querySelector('#jpgFileUploadInput');
var singleFileUploadError = document.querySelector('#singleFileUploadError');
var singleFileUploadSuccess = document.querySelector('#singleFileUploadSuccess');

function uploadSingleFile(file) {
    var formData = new FormData();
    formData.append("file", file);
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "/uploadFile");
    xhr.onload = function() {
        console.log(xhr.responseText);
        var response = JSON.parse(xhr.responseText);
        if(xhr.status == 200) {
            singleFileUploadError.style.display = "none";
            singleFileUploadSuccess.innerHTML = "<p>File Uploaded Successfully.</p><p>DownloadUrl : <a href='" + response.fileDownloadUri + "' target='_blank'>" + response.fileDownloadUri + "</a></p>";
            singleFileUploadSuccess.style.display = "block";
        } else {
            singleFileUploadSuccess.style.display = "none";
            singleFileUploadError.innerHTML = (response && response.message) || "Some Error Occurred";
        }
    }

    xhr.send(formData);
}

function createFileDetailsInDatabase(htmlFileName, jpgFileName, label) {
    var formData = new FormData();
    formData.append("title", htmlFileName);
    formData.append("photo", jpgFileName);
    formData.append("label", label);

    var xhr = new XMLHttpRequest();
        xhr.open("POST", "/savePages");

        xhr.onload = function() {
            console.log(xhr.responseText);

            if(xhr.status == 200) {
                console.log('Upload successful');
            } else {
                console.warn('Upload failed');
            }
        }

        xhr.send(formData);
}

singleUploadForm.addEventListener('submit', function(event){
    var htmfiles = htmFileUploadInput.files;
    if(htmfiles.length === 0) {
        singleFileUploadError.innerHTML = "Please select a file";
        singleFileUploadError.style.display = "block";
    }
    uploadSingleFile(htmfiles[0]);
    event.preventDefault();

   var jpgfiles = jpgFileUploadInput.files;
    if(jpgfiles.length === 0) {
        singleFileUploadError.innerHTML = "Please select a file";
        singleFileUploadError.style.display = "block";
    }
    uploadSingleFile(jpgfiles[0]);
    event.preventDefault();

    // extract htm si jpg file names
    var htmlFileName = htmfiles[0].name.split('.')[0];
    var jpgFileName = jpgfiles[0].name.split('.')[0];
    var label = document.getElementById('label').value;

    // apel endpoint savePages
    createFileDetailsInDatabase(htmlFileName, jpgFileName, label);

}, true);
